package edu.vandy.recommender.database.common.model;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Movie title and vector as stored and returned from the database
 * microservice.
 */
@Value
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Entity // For Jpa
@Table(name = "MOVIE")
public class Movie implements Comparable<Movie>
// Implement an interface that enables two Movie objects to be
// compared and checked for equality.
// TODO -- you fill in here
{
    /**
     * The movie name.
     */
    @Id
    @Column(name = "id", nullable = false)
    public String id;

    /**
     * The encoding of the movie properties.  {@link FetchType#EAGER}
     * ensures this code works with parallel stream.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    public List<Double> vector;

    /**
     * Compare this {@link Movie} with the {@code other} {@link Movie}
     * based on their IDs.
     *
     * @param other The {@link Movie} to compare to this {@link Movie}
     * @return A negative integer, zero, or a positive integer as this
     *         movie's ID is less than, equal to, or greater than the
     *         specified movie's ID
     */
    // TODO -- you fill in here.
    @Override
    public int compareTo(Movie other){
        // ensure case-insensitive
        return this.getId().compareToIgnoreCase(other.getId());
    }

    /**
     * Overrides the equals method to compare two {@link Movie}
     * objects based on their id and vector.
     *
     * @param object The {@link Object} to compare
     * @return true if the objects are equal, false otherwise.
     */
    // TODO -- you fill in here.
    // should use override because its super class has
    @Override
    public boolean equals(Object object){
        // first check if null
        // then check type if it is Movie
        // next cast type
        // then id equals
        return Optional
                .ofNullable(object)
                .filter(obj->obj instanceof Movie)
                .map(obj->(Movie) obj)
                .get()
                .getId().equals(this.getId());
    }

    @Override
    public int hashCode(){
        return id==null?0:id.hashCode();
    }
}
