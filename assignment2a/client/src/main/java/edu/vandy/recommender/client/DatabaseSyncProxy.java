package edu.vandy.recommender.client;

import edu.vandy.recommender.common.Movie;
import edu.vandy.recommender.utils.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.function.Supplier;

import static edu.vandy.recommender.utils.ExceptionUtils.rethrowSupplier;

/**
 * This class is a proxy to the {@code Movies} microservice.
 */
@Component
public class DatabaseSyncProxy {
    /**
     * Create an instance of the {@link DatabaseAPI} Retrofit client,
     * which is then used to making HTTP requests to the {@code
     * GatewayApplication} RESTful microservice.
     */
    @Autowired
    DatabaseAPI mDatabaseAPI;

    /**
     * Get a {@link List} containing the requested {@link Movie}
     * objects.
     *
     * @param route The microservice that performs the request, which
     *              is dynamically inserted into the URI via the
     *              {@code Path} annotation
     * @return An {@link Call} object that yields a {@link List}
     *         containing all the {@link Movie} objects on success and
     *         an error message on failure
     */
    List<Movie> getMovies(String route){
        // TODO -- you fill in here.
        return rethrowSupplier(() -> {
            // Execute the call.
            var response = mDatabaseAPI.getMovies(route).execute();

            // If the request is successful return the body
            // (which is a List).
            if (response.isSuccessful()) {
                return response.body();
            } else {
                // If there's a failure then find out what failed
                throw new IOException();
            }
        }).get();
    }

    /**
     * Search for movie titles in the database containing the given
     * query {@link String}.
     *
     * @param route The microservice that performs the request
     * @param query The {@link String} to search for
     * @return An {@link Call} object that yields a {@link List}
     *         containing the {@link Movie} objects that match the
     *         {@code query} on success and an error message on
     *         failure
     */
    List<Movie> searchMovies(String route,
                             String query) {
        // TODO -- you fill in here.
        return rethrowSupplier(() -> {
            // Execute the call.
            var response = mDatabaseAPI.search(route,query).execute();

            // If the request is successful return the body
            // (which is a List).
            if (response.isSuccessful()) {
                return response.body();
            } else {
                // If there's a failure then find out what failed
                throw new IOException();
            }
        }).get();
    }

    /**
     * Search for movie titles in the database containing the given
     * {@link List} of queries.
     *
     * @param route The microservice that performs the request
     * @param queries The {@link List} queries to search for
     * @return An {@link Call} object that yields a {@link List}
     *         containing the {@link Movie} objects that match the
     *         {@code queries} on success and an error message on
     *         failure
     */
    List<Movie> searchMovies(String route,
                             List<String> queries) {
        // TODO -- you fill in here.
        return rethrowSupplier(() -> {
            // Execute the call.
            var response = mDatabaseAPI.searches(route,queries).execute();

            // If the request is successful return the body
            // (which is a List).
            if (response.isSuccessful()) {
                return response.body();
            } else {
                // If there's a failure then find out what failed
                throw new IOException();
            }
        }).get();
    }

    /**
     * Get a {@link List} containing the requested {@link Movie}
     * objects.
     *
     * This endpoint also records the execution run time of this call
     * via the {@code Timer} microservice.
     *
     * @param route The microservice that performs the request, which
     *              is dynamically inserted into the URI via the
     *              {@code Path} annotation
     * @return An {@link Call} object that yields a {@link List}
     *         containing all the {@link Movie} objects on success and
     *         an error message on failure
     */
    List<Movie> getMoviesTimed(String route) {
        // TODO -- you fill in here.
        try {
            var movie_resp=mDatabaseAPI.getMoviesTimed(route).execute();
            // I do not know if it is correct and I asked chatGPT to give me the hint
            return ExceptionUtils
                    .rethrowSupplier(movie_resp::body)
                    .get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Search for movie titles in the database containing the given
     * query {@link String}.
     *
     * This endpoint also records the execution run time of this call
     * via the {@code Timer} microservice.
     *
     * @param route The microservice that performs the request
     * @param query The {@link String} to search for
     * @return An {@link Call} object that yields a {@link List}
     *         containing the {@link Movie} objects that match the
     *         {@code query} on success and an error message on
     *         failure
     */
    List<Movie> searchMoviesTimed(String route,
                                  String query) {
        // TODO -- you fill in here.
        try {
            var movie_resp=mDatabaseAPI.searchMoviesTimed(route,query).execute();
            // I do not know if it is correct and I asked chatGPT to give me the hint
            return ExceptionUtils
                    .rethrowSupplier(movie_resp::body)
                    .get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Search for movie titles in the database containing the given
     * {@link List} of queries.
     *
     * This endpoint also records the execution run time of this call
     * via the {@code Timer} microservice.
     *
     * @param route The microservice that performs the request
     * @param queries The {@link List} queries to search for
     * @return An {@link Call} object that yields a {@link List}
     *         containing the {@link Movie} objects that match the
     *         {@code queries} on success and an error message on
     *         failure
     */
    List<Movie> searchMoviesTimed(String route,
                                  List<String> queries) {
        // TODO -- you fill in here.
        try {
            var movie_resp=mDatabaseAPI.searchesMoviesTimed(route,queries).execute();
            // I do not know if it is correct and I asked chatGPT to give me the hint
            return ExceptionUtils
                    .rethrowSupplier(movie_resp::body)
                    .get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

